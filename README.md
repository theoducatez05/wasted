# Wasted

Global Game Jam link : [https://globalgamejam.org/2020/games/wasted-4](https://globalgamejam.org/2020/games/wasted-4)

## Table of Contents

* [About the Game](#about-the-game)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)  
  * [Screenshots](#screenshots)
* [Creators](#creators)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)




## About The Game

![The main menu of the game](GitLab_images/main_menu.PNG "the main menu")

You are a teenager throwing an amazing party at your house. You invited a lot of friends and everybody is having a great time until some of your guests start to go crazy and break everything in the appartment. You will have to do everything possible to repair the mess they are doing before you parents come back or the police gets called. Don't let the white gauge fill up entierly until the timer finishes or you are lost. If you succeed in cleaning everything up and stop the stress gauge to fill up until the timer finishes, you win ! Your parents wont even see that you threw the party of the year !

Be carefull, you guests are pretty imaginative about the mess they can create. They can break your parents objects, move your furnitures, lit a fire in the middle of the room, puke everywhere, smoke in the appartment and drug you ! So Repair what they break, move back what they moved, put out the fire, clean when they puke everywhere, try to see through the clouds of smoke and avoid getting drugged !

Wasted is a 3D video game made during the 2020 Global Game Jam in Paris. The goal was to create a video game in two days with a specific theme given to all participants. Our team was composed of 2 game programmer, 5 artists, 1 game designer and 1 sound designer. We are all passionate about video games or are currently studying on this domain. We worked together to produce this fun video game and are happy with the result. The theme of the 2020 edition was "repair"

### Built With

* [Unity](https://unity.com/)
* [Maya](https://unity.com/)
* [FMOD](https://www.fmod.com/)
* A lot of hard work !



## Getting Started

How to play the game :

### Prerequisites

To play the game you only need a PC with Windows to run it. The game is very small so you don't need high specs.

### Installation

1. Clone the repo
```sh
git clone https://gitlab.com/theoducatez05/wasted.git
```
2. Launch the .exe file in the directory



## Usage

### Screenshots

That is how the main menu looks like :

![The main menu of the game](GitLab_images/main_menu.PNG "the main menu")

You have to repair objects your guests break :
<div align="center">
![Object repairing](GitLab_images/wasted_qqmypwhgxj.jpg "Object repairing")
</div>

You have to clean all this mess :

![Clean the mess](GitLab_images/game2.PNG "Clean the mess")

You even have to put down fire !

![Put down fire](GitLab_images/fire.PNG "Put down fire")

Sometimes it gets very chaotic :

![Chaos](GitLab_images/chaos.PNG "Chaos")

And finally if you don't succes in cleaning everything quickly, you loose :(

![You loose](GitLab_images/loose.PNG "You loose")

## Creators

* Jade Perdrillat (3D Video game Artist student)
* Marie Uzan (3D Video game Artist student)
* Alexis Facque (Video game Artist student)
* Thibaut Husson (3D Artist student)
* Lucas Ruyer (Game designer student)
* Hugo Munini (Game programmer student)
* Sadi Wassel (Sound designer student)
* Théo Ducatez (Computer engineering student)

## Contact

Théo Ducatez - [theo-ducatez](https://www.linkedin.com/in/theo-ducatez/) - theoducatez05@gmail.com

Project Link: [https://gitlab.com/theoducatez05/wasted](https://gitlab.com/theoducatez05/wasted)



## Acknowledgements
* [Global Game Jam](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Unity](https://unity.com/)
* [ISART Paris](https://www.isart.com/)
